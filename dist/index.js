"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _cool = require("./actions/cool");

Object.defineProperty(exports, "Cool", {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_cool).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }